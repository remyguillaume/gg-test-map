import TileLayer from "ol/layer/Tile";
import { render } from "uhtml";
import { Hole } from "uhtml/keyed";

import Map from 'ol/Map';
import OSM from 'ol/source/OSM';
import { View } from "ol";
import OLCesium from 'olcs/OLCesium';
import { Cesium3DTileset, CesiumTerrainProvider, Scene } from "cesium";

import proj4 from 'proj4';
import { register } from 'ol/proj/proj4';

class MyMap extends HTMLElement {
  template?: () => Hole;

  templateUrl = './template.html';
  styleUrl = './style.css';

  shadow: ShadowRoot;
  scene: Scene | null = null;

  constructor() {
    super();
    this.shadow = this.attachShadow({ mode: 'open' });

    // Default configuration for Cesium
    // See https://cesium.com/learn/cesiumjs-learn/cesiumjs-quickstart/
    // @ts-ignore
    window.CESIUM_BASE_URL = 'https://cesium.com/downloads/cesiumjs/releases/1.111/Build/Cesium/';

    // Register custom EPSG (https://epsg.io/2056)
    proj4.defs("EPSG:2056", '+proj=somerc +lat_0=46.9524055555556 +lon_0=7.43958333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs +type=crs');
    register(proj4);
  }

  connectedCallback() {
    render(this.shadowRoot, this.template!);

    const mapTarget = this.shadow.getElementById('ol-map') as HTMLDivElement;
    const map = new Map({
      layers: [
        new TileLayer({
          source: new OSM(),
        }),
      ],
      target: mapTarget
    });

    const view = new View({
      center: [2611000, 1267000],
      zoom: 4,
      extent: [2583000, 1235000, 2650000, 1291000],
      resolutions: [52.91677250021167, 19.843789687579378, 10.583354500042335, 5.291677250021167, 2.6458386250105836, 1.9843789687579376, 1.3229193125052918, 0.9260435187537043, 0.5291677250021167, 0.26458386250105836, 0.13229193125052918, 0.05291677250021167, 0.02645838625010583, 0.013229193125052918],
      projection: 'EPSG:2056',
      constrainResolution: true
    });

    map.setView(view);

    const map3dTarget = this.shadow.getElementById('cs-map') as HTMLDivElement;
    const ol3d = new OLCesium({ map: map, target: map3dTarget });
    ol3d.setEnabled(true);
    this.scene = ol3d.getCesiumScene();
    this.load();
  }

  async load() {

    const terrainProvider = await CesiumTerrainProvider.fromUrl('https://terrain100.geo.admin.ch/1.0.0/ch.swisstopo.terrain.3d/');
    this.scene!.terrainProvider = terrainProvider;

    const tileset = await Cesium3DTileset.fromUrl('https://vectortiles100.geo.admin.ch/3d-tiles/ch.swisstopo.swisstlm3d.3d/20201020/tileset.json');
    this.scene!.primitives.add(tileset);
  }

}

export default MyMap;
